<patch-1.0>
   <obj type="patch/inlet b" sha="17c8e188371661163bfa55cea9974eecb785fb06" uuid="3b0d3eacb5bb978cb05d1372aa2714d5a4790844" name="Trigger" x="28" y="70">
      <params/>
      <attribs/>
   </obj>
   <obj type="env/d" sha="d9f7cfe1295d7bcc550714a18126d4f73c7c8411" uuid="190ae648e41832b41adbedb465317c18a010aefe" name="d_1" x="154" y="70">
      <params>
         <frac32.s.map name="d" value="-14.0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="osc/sine" sha="edec4a9d5f533ea748cd564ce8c69673dd78742f" uuid="6e094045cca76a9dbf7ebfa72e44e4700d2b3ba" name="sine_1" x="294" y="70">
      <params>
         <frac32.s.map name="pitch" value="-28.0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="math/+" sha="9f8cb68e327acfaa43539be73ff97faab497ad45" uuid="faedbea4612d9bd3644d6d3bf31946d848a70e19" name="+_1" x="462" y="140">
      <params/>
      <attribs/>
   </obj>
   <obj type="gain/vca" sha="c904cdd24d65968df2f5796e107db3747dd691a6" uuid="a9f2dcd18043e2f47364e45cb8814f63c2a37c0d" name="vca_1" x="588" y="168">
      <params/>
      <attribs/>
   </obj>
   <obj type="patch/outlet a" sha="9e7e04867e1d37837b0924c9bf18c44ac68602e6" uuid="abd8c5fd3b0524a6630f65cad6dc27f6c58e2a3e" name="Audio Out" x="686" y="182">
      <params/>
      <attribs/>
   </obj>
   <obj type="osc/sine" sha="edec4a9d5f533ea748cd564ce8c69673dd78742f" uuid="6e094045cca76a9dbf7ebfa72e44e4700d2b3ba" name="Range: F2 to C3" x="294" y="210">
      <params>
         <frac32.s.map name="pitch" onParent="true" value="-16.0"/>
      </params>
      <attribs/>
   </obj>
   <comment type="patch/comment" x="294" y="336" text="Tune this oscillator anywhere in the range of F2 to C3"/>
   <comment type="patch/comment" x="294" y="350" text="F2 will give you a low tom"/>
   <comment type="patch/comment" x="294" y="364" text="C3 will give you a high tom"/>
   <nets>
      <net>
         <source obj="sine_1" outlet="wave"/>
         <dest obj="+_1" inlet="in1"/>
      </net>
      <net>
         <source obj="Range: F2 to C3" outlet="wave"/>
         <dest obj="+_1" inlet="in2"/>
      </net>
      <net>
         <source obj="+_1" outlet="out"/>
         <dest obj="vca_1" inlet="a"/>
      </net>
      <net>
         <source obj="d_1" outlet="env"/>
         <dest obj="vca_1" inlet="v"/>
      </net>
      <net>
         <source obj="Trigger" outlet="inlet"/>
         <dest obj="d_1" inlet="trig"/>
      </net>
      <net>
         <source obj="vca_1" outlet="o"/>
         <dest obj="Audio Out" inlet="outlet"/>
      </net>
   </nets>
   <settings>
      <subpatchmode>normal</subpatchmode>
      <MidiChannel>1</MidiChannel>
      <HasMidiChannelSelector>true</HasMidiChannelSelector>
      <NPresets>8</NPresets>
      <NPresetEntries>32</NPresetEntries>
      <NModulationSources>8</NModulationSources>
      <NModulationTargetsPerSource>8</NModulationTargetsPerSource>
      <Author>Jan Vantomme</Author>
      <License>LGPL</License>
   </settings>
   <notes><![CDATA[]]></notes>
   <windowPos>
      <x>39</x>
      <y>31</y>
      <width>1332</width>
      <height>845</height>
   </windowPos>
</patch-1.0>