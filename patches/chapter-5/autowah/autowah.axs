<patch-1.0>
   <obj type="ctrl/toggle" sha="f5742cc9eee76fae90a4e570c34596dd327b6c28" uuid="42b8134fa729d54bfc8d62d6ef3fa99498c1de99" name="Effect ON" x="532" y="28">
      <params>
         <bool32.tgl name="b" onParent="true" value="1"/>
      </params>
      <attribs/>
   </obj>
   <obj type="patch/inlet a" sha="709c10aa648c6e5a3c00da4b5dd238899a7c109c" uuid="b577fe41e0a6bc7b5502ce33cb8a3129e2e28ee5" name="inlet_1" x="28" y="70">
      <params/>
      <attribs/>
   </obj>
   <obj type="env/follower" sha="2f512222f4b4d2c035c326cd486c1cacc6be5e64" uuid="356ce1d18ac6b51704833f94dac5dea823fb8223" name="follower_1" x="126" y="70">
      <params/>
      <attribs>
         <combo attributeName="time" selection="5.3ms"/>
      </attribs>
   </obj>
   <obj type="math/gain" sha="479fc45a4d36c8ec5e891834d52fa725d1c27de7" uuid="6b4dd3da49f98e54900f6c20031f38f4624fa364" name="gain_1" x="238" y="70">
      <params>
         <frac32.u.map name="amp" value="53.5"/>
      </params>
      <attribs/>
   </obj>
   <obj type="math/smooth" sha="74c40dd64acc980ba4e6ffc729e07365b00044fa" uuid="6c5d08c282bb08bff24af85b4891447f99bcbc97" name="smooth_1" x="364" y="112">
      <params>
         <frac32.u.map name="time" value="23.5"/>
      </params>
      <attribs/>
   </obj>
   <obj type="filter/vcf3" sha="2a5cccf4517f54d2450ab7518925f49e4c41c837" uuid="92455c652cd098cbb682a5497baa18abbf2ef865" name="vcf3_1" x="532" y="126">
      <params>
         <frac32.s.map name="pitch" onParent="true" value="-13.0"/>
         <frac32.u.map name="reso" onParent="true" value="50.0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="mux/mux 2" sha="10c5b6d774e8c972b6bb863dad23e83034e0990f" uuid="539c246f4c360ac476e128cfbfa84348fb7f7e73" name="mux_2" x="700" y="168">
      <params/>
      <attribs/>
   </obj>
   <obj type="patch/outlet a" sha="9e7e04867e1d37837b0924c9bf18c44ac68602e6" uuid="abd8c5fd3b0524a6630f65cad6dc27f6c58e2a3e" name="outlet_1" x="798" y="182">
      <params/>
      <attribs/>
   </obj>
   <nets>
      <net>
         <source obj="Effect ON" outlet="o"/>
         <dest obj="mux_2" inlet="s"/>
      </net>
      <net>
         <source obj="inlet_1" outlet="inlet"/>
         <dest obj="mux_2" inlet="i1"/>
         <dest obj="follower_1" inlet="in"/>
         <dest obj="vcf3_1" inlet="in"/>
      </net>
      <net>
         <source obj="vcf3_1" outlet="out"/>
         <dest obj="mux_2" inlet="i2"/>
      </net>
      <net>
         <source obj="gain_1" outlet="out"/>
         <dest obj="smooth_1" inlet="in"/>
      </net>
      <net>
         <source obj="follower_1" outlet="amp"/>
         <dest obj="gain_1" inlet="in"/>
      </net>
      <net>
         <source obj="smooth_1" outlet="out"/>
         <dest obj="vcf3_1" inlet="pitch"/>
      </net>
      <net>
         <source obj="mux_2" outlet="o"/>
         <dest obj="outlet_1" inlet="outlet"/>
      </net>
   </nets>
   <settings>
      <subpatchmode>normal</subpatchmode>
      <MidiChannel>1</MidiChannel>
      <NPresets>8</NPresets>
      <NPresetEntries>32</NPresetEntries>
      <NModulationSources>8</NModulationSources>
      <NModulationTargetsPerSource>8</NModulationTargetsPerSource>
      <Author></Author>
   </settings>
   <notes><![CDATA[]]></notes>
   <windowPos>
      <x>47</x>
      <y>25</y>
      <width>1356</width>
      <height>800</height>
   </windowPos>
</patch-1.0>