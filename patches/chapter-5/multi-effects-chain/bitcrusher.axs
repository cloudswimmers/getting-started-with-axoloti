<patch-1.0>
   <obj type="ctrl/toggle" sha="f5742cc9eee76fae90a4e570c34596dd327b6c28" uuid="42b8134fa729d54bfc8d62d6ef3fa99498c1de99" name="Effect ON" x="322" y="28">
      <params>
         <bool32.tgl name="b" onParent="true" value="0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="patch/inlet a" sha="709c10aa648c6e5a3c00da4b5dd238899a7c109c" uuid="b577fe41e0a6bc7b5502ce33cb8a3129e2e28ee5" name="Audio In" x="28" y="70">
      <params/>
      <attribs/>
   </obj>
   <obj type="math/quantize" sha="9dea6860d04c1ab0b7e0abcfca2b4b454a79b76a" uuid="c2e999efc45cf8c5077dd1b140dcc793221e050e" name="Crush" x="140" y="84">
      <params/>
      <attribs>
         <spinner attributeName="bits" value="1"/>
      </attribs>
   </obj>
   <obj type="mux/mux 2" sha="10c5b6d774e8c972b6bb863dad23e83034e0990f" uuid="539c246f4c360ac476e128cfbfa84348fb7f7e73" name="mux_1" x="322" y="98">
      <params/>
      <attribs/>
   </obj>
   <obj type="patch/outlet a" sha="9e7e04867e1d37837b0924c9bf18c44ac68602e6" uuid="abd8c5fd3b0524a6630f65cad6dc27f6c58e2a3e" name="Audio Out" x="462" y="112">
      <params/>
      <attribs/>
   </obj>
   <nets>
      <net>
         <source obj="Effect ON" outlet="o"/>
         <dest obj="mux_1" inlet="s"/>
      </net>
      <net>
         <source obj="Audio In" outlet="inlet"/>
         <dest obj="mux_1" inlet="i1"/>
         <dest obj="Crush" inlet="a"/>
      </net>
      <net>
         <source obj="Crush" outlet="b"/>
         <dest obj="mux_1" inlet="i2"/>
      </net>
      <net>
         <source obj="mux_1" outlet="o"/>
         <dest obj="Audio Out" inlet="outlet"/>
      </net>
   </nets>
   <settings>
      <subpatchmode>normal</subpatchmode>
      <MidiChannel>1</MidiChannel>
      <NPresets>8</NPresets>
      <NPresetEntries>32</NPresetEntries>
      <NModulationSources>8</NModulationSources>
      <NModulationTargetsPerSource>8</NModulationTargetsPerSource>
   </settings>
   <notes><![CDATA[]]></notes>
   <windowPos>
      <x>117</x>
      <y>22</y>
      <width>1290</width>
      <height>874</height>
   </windowPos>
</patch-1.0>