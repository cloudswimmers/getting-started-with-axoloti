<patch-1.0>
   <obj type="patch/inlet b" sha="17c8e188371661163bfa55cea9974eecb785fb06" uuid="3b0d3eacb5bb978cb05d1372aa2714d5a4790844" name="Trigger" x="28" y="56">
      <params/>
      <attribs/>
   </obj>
   <obj type="ctrl/i radio 4 v" sha="37aa6f17297981ccdf9a037eecf728e7c0ff0ae4" uuid="b610704137c90b0e43464440b84bfb4fb7d2bb30" name="Select Sample" x="28" y="112">
      <params>
         <int32.vradio name="value" onParent="true" value="0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="wave/play" sha="766385f1919d1fbf35897f3a145eff88a9466101" uuid="2e6265136dbaaf4d885a669520546de2e3306819" name="play_2" x="322" y="168">
      <params/>
      <attribs/>
   </obj>
   <obj type="patch/outlet a" sha="9e7e04867e1d37837b0924c9bf18c44ac68602e6" uuid="abd8c5fd3b0524a6630f65cad6dc27f6c58e2a3e" name="Audio Out" x="448" y="182">
      <params/>
      <attribs/>
   </obj>
   <obj type="string/c" sha="914b3f01295e2c5023504807620dd88cfeaa8907" uuid="4aa90a90c435a742ddfa152d232883fc5b2f1b3" name="c_1" x="28" y="224">
      <params/>
      <attribs>
         <table attributeName="str" table="male.raw"/>
      </attribs>
   </obj>
   <obj type="mux/mux 4" sha="4b0b83f81c6843e37c292240914da8014b6ccda" uuid="4629dfad262ff68419d12ab3fcd96e5e2e9f4190" name="mux_1" x="196" y="224">
      <params/>
      <attribs/>
   </obj>
   <obj type="string/c" sha="914b3f01295e2c5023504807620dd88cfeaa8907" uuid="4aa90a90c435a742ddfa152d232883fc5b2f1b3" name="c_2" x="28" y="308">
      <params/>
      <attribs>
         <table attributeName="str" table="female.raw"/>
      </attribs>
   </obj>
   <obj type="string/c" sha="914b3f01295e2c5023504807620dd88cfeaa8907" uuid="4aa90a90c435a742ddfa152d232883fc5b2f1b3" name="c_3" x="28" y="392">
      <params/>
      <attribs>
         <table attributeName="str" table="guitar1.raw"/>
      </attribs>
   </obj>
   <obj type="string/c" sha="914b3f01295e2c5023504807620dd88cfeaa8907" uuid="4aa90a90c435a742ddfa152d232883fc5b2f1b3" name="c_4" x="28" y="476">
      <params/>
      <attribs>
         <table attributeName="str" table="drumloop.raw"/>
      </attribs>
   </obj>
   <nets>
      <net>
         <source obj="mux_1" outlet="o"/>
         <dest obj="play_2" inlet="filename"/>
      </net>
      <net>
         <source obj="c_1" outlet="out"/>
         <dest obj="mux_1" inlet="i0"/>
      </net>
      <net>
         <source obj="c_2" outlet="out"/>
         <dest obj="mux_1" inlet="i1"/>
      </net>
      <net>
         <source obj="c_3" outlet="out"/>
         <dest obj="mux_1" inlet="i2"/>
      </net>
      <net>
         <source obj="c_4" outlet="out"/>
         <dest obj="mux_1" inlet="i3"/>
      </net>
      <net>
         <source obj="Select Sample" outlet="out"/>
         <dest obj="mux_1" inlet="s"/>
      </net>
      <net>
         <source obj="play_2" outlet="out"/>
         <dest obj="Audio Out" inlet="outlet"/>
      </net>
      <net>
         <source obj="Trigger" outlet="inlet"/>
         <dest obj="play_2" inlet="start"/>
      </net>
   </nets>
   <settings>
      <subpatchmode>no</subpatchmode>
   </settings>
   <notes><![CDATA[]]></notes>
   <windowPos>
      <x>157</x>
      <y>43</y>
      <width>1247</width>
      <height>853</height>
   </windowPos>
</patch-1.0>