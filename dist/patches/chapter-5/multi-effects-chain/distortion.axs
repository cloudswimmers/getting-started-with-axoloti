<patch-1.0>
   <obj type="ctrl/toggle" sha="f5742cc9eee76fae90a4e570c34596dd327b6c28" uuid="42b8134fa729d54bfc8d62d6ef3fa99498c1de99" name="Effect ON" x="42" y="28">
      <params>
         <bool32.tgl name="b" onParent="true" value="0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="ctrl/i radio 4 v" sha="37aa6f17297981ccdf9a037eecf728e7c0ff0ae4" uuid="b610704137c90b0e43464440b84bfb4fb7d2bb30" name="Distortion Type" x="168" y="28">
      <params>
         <int32.vradio name="value" onParent="true" value="0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="patch/inlet a" sha="709c10aa648c6e5a3c00da4b5dd238899a7c109c" uuid="b577fe41e0a6bc7b5502ce33cb8a3129e2e28ee5" name="Audio In" x="42" y="140">
      <params/>
      <attribs/>
   </obj>
   <obj type="math/gain" sha="cda56a9f0ea80746e47be1a82a59ebc55bdba5d1" uuid="62b1c1a6337c7c8f6aec96408a432477b113cfa0" name="Gain" x="168" y="140">
      <params>
         <frac32.u.map name="amp" onParent="true" value="52.0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="dist/rectifier full" sha="9c0ffac0e17fe7ea4f6eb000ca93f76f67240900" uuid="bdd28e370b58801ee42bcae7bcdf8602d6181602" name="rectifier_1" x="308" y="140">
      <params/>
      <attribs/>
   </obj>
   <obj type="mux/mux 4" sha="9f7f3b7a0abf760b335371219c835086f87c62b0" uuid="e511105cf5630d1a0b4a144dc3fabb3cc7c07bd" name="mux_3" x="434" y="182">
      <params/>
      <attribs/>
   </obj>
   <obj type="dist/rectifier" sha="dd59319d5d9fb8412e8bc1ce69ec96277160cf0c" uuid="a994d72e4491dedd2655b7a06a4a3f38fcca68d2" name="rectifier_2" x="308" y="196">
      <params/>
      <attribs/>
   </obj>
   <obj type="math/*c" sha="a73a2cafcf92eb4b6524655dcee98a569d5ddc28" uuid="7a66f52a9594e7e9eb31328ea725cb3641a80b55" name="Level" x="518" y="196">
      <params>
         <frac32.u.map name="amp" onParent="true" value="49.0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="dist/soft" sha="74960c930c4b6a5c630156778f889d4de48dbdbf" uuid="e680d76a805e4866027cdf654c7efd8b2e54622" name="soft_1" x="308" y="252">
      <params/>
      <attribs/>
   </obj>
   <obj type="dist/hardclip" uuid="8d73ded73a7cb73e0dc71f6fc90f45191c734e50" name="hardclip_1" x="308" y="308">
      <params>
         <frac32.u.map name="level" onParent="true" value="4.5"/>
      </params>
      <attribs/>
   </obj>
   <obj type="mux/mux 2" sha="10c5b6d774e8c972b6bb863dad23e83034e0990f" uuid="539c246f4c360ac476e128cfbfa84348fb7f7e73" name="mux_2" x="644" y="308">
      <params/>
      <attribs/>
   </obj>
   <obj type="patch/outlet a" sha="9e7e04867e1d37837b0924c9bf18c44ac68602e6" uuid="abd8c5fd3b0524a6630f65cad6dc27f6c58e2a3e" name="Audio Out" x="756" y="336">
      <params/>
      <attribs/>
   </obj>
   <nets>
      <net>
         <source obj="Effect ON" outlet="o"/>
         <dest obj="mux_2" inlet="s"/>
      </net>
      <net>
         <source obj="Audio In" outlet="inlet"/>
         <dest obj="Gain" inlet="in"/>
         <dest obj="mux_2" inlet="i1"/>
      </net>
      <net>
         <source obj="Gain" outlet="out"/>
         <dest obj="rectifier_1" inlet="in"/>
         <dest obj="rectifier_2" inlet="in"/>
         <dest obj="soft_1" inlet="in"/>
         <dest obj="hardclip_1" inlet="in"/>
      </net>
      <net>
         <source obj="Level" outlet="out"/>
         <dest obj="mux_2" inlet="i2"/>
      </net>
      <net>
         <source obj="rectifier_1" outlet="out"/>
         <dest obj="mux_3" inlet="i0"/>
      </net>
      <net>
         <source obj="rectifier_2" outlet="out"/>
         <dest obj="mux_3" inlet="i1"/>
      </net>
      <net>
         <source obj="soft_1" outlet="out"/>
         <dest obj="mux_3" inlet="i2"/>
      </net>
      <net>
         <source obj="hardclip_1" outlet="outlet_1"/>
         <dest obj="mux_3" inlet="i3"/>
      </net>
      <net>
         <source obj="mux_3" outlet="o"/>
         <dest obj="Level" inlet="in"/>
      </net>
      <net>
         <source obj="Distortion Type" outlet="out"/>
         <dest obj="mux_3" inlet="s"/>
      </net>
      <net>
         <source obj="mux_2" outlet="o"/>
         <dest obj="Audio Out" inlet="outlet"/>
      </net>
   </nets>
   <settings>
      <subpatchmode>normal</subpatchmode>
      <MidiChannel>1</MidiChannel>
      <NPresets>8</NPresets>
      <NPresetEntries>32</NPresetEntries>
      <NModulationSources>8</NModulationSources>
      <NModulationTargetsPerSource>8</NModulationTargetsPerSource>
      <Author>Jan Vantomme</Author>
      <License>LGPL</License>
      <Attributions></Attributions>
   </settings>
   <notes><![CDATA[]]></notes>
   <windowPos>
      <x>20</x>
      <y>72</y>
      <width>1356</width>
      <height>800</height>
   </windowPos>
</patch-1.0>