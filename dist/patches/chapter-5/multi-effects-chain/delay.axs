<patch-1.0>
   <obj type="patch/inlet a" sha="709c10aa648c6e5a3c00da4b5dd238899a7c109c" uuid="b577fe41e0a6bc7b5502ce33cb8a3129e2e28ee5" name="Audio In" x="28" y="42">
      <params/>
      <attribs/>
   </obj>
   <obj type="delay/write sdram" sha="63d300dd732507b7237b5fd45676bc14c9e77ce7" uuid="5ae03f8d7b815edcfc40585d8bbac2ed48460fba" name="delay1" x="126" y="42">
      <params/>
      <attribs>
         <combo attributeName="size" selection="4096 (85.33ms)"/>
      </attribs>
   </obj>
   <obj type="delay/read interp" sha="6fda3a4b04cc8fc49e63240c2fff115695ec7a7" uuid="e3d8b8823ab551c588659520bf6cc25c630466c7" name="read_1" x="252" y="42">
      <params>
         <frac32.u.map name="time" value="0.0"/>
      </params>
      <attribs>
         <objref attributeName="delayname" obj="delay1"/>
      </attribs>
   </obj>
   <obj type="ctrl/toggle" sha="f5742cc9eee76fae90a4e570c34596dd327b6c28" uuid="42b8134fa729d54bfc8d62d6ef3fa99498c1de99" name="Effect ON" x="420" y="42">
      <params>
         <bool32.tgl name="b" onParent="true" value="1"/>
      </params>
      <attribs/>
   </obj>
   <obj type="delay/write sdram" sha="63d300dd732507b7237b5fd45676bc14c9e77ce7" uuid="5ae03f8d7b815edcfc40585d8bbac2ed48460fba" name="delay2" x="126" y="98">
      <params/>
      <attribs>
         <combo attributeName="size" selection="8192 (170ms)"/>
      </attribs>
   </obj>
   <obj type="delay/read interp" sha="6fda3a4b04cc8fc49e63240c2fff115695ec7a7" uuid="e3d8b8823ab551c588659520bf6cc25c630466c7" name="read_2" x="252" y="140">
      <params>
         <frac32.u.map name="time" value="0.0"/>
      </params>
      <attribs>
         <objref attributeName="delayname" obj="delay2"/>
      </attribs>
   </obj>
   <obj type="delay/write sdram" sha="63d300dd732507b7237b5fd45676bc14c9e77ce7" uuid="5ae03f8d7b815edcfc40585d8bbac2ed48460fba" name="delay3" x="126" y="154">
      <params/>
      <attribs>
         <combo attributeName="size" selection="16384 (341ms)"/>
      </attribs>
   </obj>
   <obj type="mix/mix 4" sha="6d667381bdeea6a139000a94f808f3e63efd414a" uuid="e6f9a0cc7aadc1b89516143cf1ccb79b3538d05a" name="mix_1" x="434" y="182">
      <params>
         <frac32.u.map name="gain1" value="52.5"/>
         <frac32.u.map name="gain2" value="51.5"/>
         <frac32.u.map name="gain3" value="57.0"/>
         <frac32.u.map name="gain4" value="63.0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="delay/write sdram" sha="63d300dd732507b7237b5fd45676bc14c9e77ce7" uuid="5ae03f8d7b815edcfc40585d8bbac2ed48460fba" name="delay4" x="126" y="210">
      <params/>
      <attribs>
         <combo attributeName="size" selection="32768 (682ms)"/>
      </attribs>
   </obj>
   <obj type="delay/read interp" sha="6fda3a4b04cc8fc49e63240c2fff115695ec7a7" uuid="e3d8b8823ab551c588659520bf6cc25c630466c7" name="read_3" x="252" y="238">
      <params>
         <frac32.u.map name="time" value="0.0"/>
      </params>
      <attribs>
         <objref attributeName="delayname" obj="delay3"/>
      </attribs>
   </obj>
   <obj type="ctrl/dial p" sha="501c30e07dedf3d701e8d0b33c3c234908c3388e" uuid="cc5d2846c3d50e425f450c4b9851371b54f4d674" name="Dry/Wet" x="28" y="280">
      <params>
         <frac32.u.map name="value" onParent="true" value="57.5"/>
      </params>
      <attribs/>
   </obj>
   <obj type="conv/interp" sha="4b93f3e0f08b85924e07feabcdfe53fc11aa6a48" uuid="d68c1a8709d8b55e3de8715d727ec0a2d8569d9a" name="interp_1" x="126" y="280">
      <params/>
      <attribs/>
   </obj>
   <obj type="delay/read interp" sha="6fda3a4b04cc8fc49e63240c2fff115695ec7a7" uuid="e3d8b8823ab551c588659520bf6cc25c630466c7" name="read_4" x="252" y="336">
      <params>
         <frac32.u.map name="time" value="0.0"/>
      </params>
      <attribs>
         <objref attributeName="delayname" obj="delay4"/>
      </attribs>
   </obj>
   <obj type="mux/mux 2" sha="10c5b6d774e8c972b6bb863dad23e83034e0990f" uuid="539c246f4c360ac476e128cfbfa84348fb7f7e73" name="mux_1" x="560" y="364">
      <params/>
      <attribs/>
   </obj>
   <obj type="patch/outlet a" sha="9e7e04867e1d37837b0924c9bf18c44ac68602e6" uuid="abd8c5fd3b0524a6630f65cad6dc27f6c58e2a3e" name="Audio Out" x="672" y="392">
      <params/>
      <attribs/>
   </obj>
   <nets>
      <net>
         <source obj="Effect ON" outlet="o"/>
         <dest obj="mux_1" inlet="s"/>
      </net>
      <net>
         <source obj="Audio In" outlet="inlet"/>
         <dest obj="mux_1" inlet="i1"/>
         <dest obj="mix_1" inlet="bus_in"/>
         <dest obj="delay1" inlet="in"/>
         <dest obj="delay2" inlet="in"/>
         <dest obj="delay3" inlet="in"/>
         <dest obj="delay4" inlet="in"/>
      </net>
      <net>
         <source obj="read_1" outlet="out"/>
         <dest obj="mix_1" inlet="in1"/>
      </net>
      <net>
         <source obj="read_2" outlet="out"/>
         <dest obj="mix_1" inlet="in2"/>
      </net>
      <net>
         <source obj="mix_1" outlet="out"/>
         <dest obj="mux_1" inlet="i2"/>
      </net>
      <net>
         <source obj="read_3" outlet="out"/>
         <dest obj="mix_1" inlet="in3"/>
      </net>
      <net>
         <source obj="read_4" outlet="out"/>
         <dest obj="mix_1" inlet="in4"/>
      </net>
      <net>
         <source obj="Dry/Wet" outlet="out"/>
         <dest obj="interp_1" inlet="i"/>
      </net>
      <net>
         <source obj="interp_1" outlet="o"/>
         <dest obj="read_1" inlet="time"/>
         <dest obj="read_2" inlet="time"/>
         <dest obj="read_3" inlet="time"/>
         <dest obj="read_4" inlet="time"/>
      </net>
      <net>
         <source obj="mux_1" outlet="o"/>
         <dest obj="Audio Out" inlet="outlet"/>
      </net>
   </nets>
   <settings>
      <subpatchmode>normal</subpatchmode>
      <MidiChannel>1</MidiChannel>
      <NPresets>8</NPresets>
      <NPresetEntries>32</NPresetEntries>
      <NModulationSources>8</NModulationSources>
      <NModulationTargetsPerSource>8</NModulationTargetsPerSource>
      <Author>Jan Vantomme</Author>
      <License>LGPL</License>
      <Attributions></Attributions>
   </settings>
   <notes><![CDATA[]]></notes>
   <windowPos>
      <x>253</x>
      <y>307</y>
      <width>1290</width>
      <height>874</height>
   </windowPos>
</patch-1.0>