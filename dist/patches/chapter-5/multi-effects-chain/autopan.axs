<patch-1.0>
   <obj type="ctrl/toggle" sha="f5742cc9eee76fae90a4e570c34596dd327b6c28" uuid="42b8134fa729d54bfc8d62d6ef3fa99498c1de99" name="Effect ON" x="140" y="14">
      <params>
         <bool32.tgl name="b" onParent="true" value="0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="patch/inlet a" sha="709c10aa648c6e5a3c00da4b5dd238899a7c109c" uuid="b577fe41e0a6bc7b5502ce33cb8a3129e2e28ee5" name="Audio In" x="14" y="84">
      <params/>
      <attribs/>
   </obj>
   <obj type="demux/demux 2" sha="31f846b7eaf1a57b5edcb94d67ceadda73b38d2b" uuid="f48c5f63c31c60c077648e2a0886e592e4fa9434" name="demux_1" x="112" y="84">
      <params/>
      <attribs/>
   </obj>
   <obj type="spat/pan m" sha="f7012465833160bfc83de84d1271874269790807" uuid="f6c6c0d7224841d9d76962e64d7f779d8194b7f9" name="pan_1" x="280" y="126">
      <params/>
      <attribs/>
   </obj>
   <obj type="mux/mux 2" sha="10c5b6d774e8c972b6bb863dad23e83034e0990f" uuid="539c246f4c360ac476e128cfbfa84348fb7f7e73" name="mux_2" x="434" y="140">
      <params/>
      <attribs/>
   </obj>
   <obj type="patch/outlet a" sha="9e7e04867e1d37837b0924c9bf18c44ac68602e6" uuid="abd8c5fd3b0524a6630f65cad6dc27f6c58e2a3e" name="Audio Out Left" x="532" y="168">
      <params/>
      <attribs/>
   </obj>
   <obj type="lfo/sine" sha="a2851b3d62ed0faceefc98038d9571422f0ce260" uuid="75f7330c26a13953215dccc3b7b9008545c9daa9" name="Speed" x="112" y="196">
      <params>
         <frac32.s.map name="pitch" onParent="true" value="-32.0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="mux/mux 2" sha="10c5b6d774e8c972b6bb863dad23e83034e0990f" uuid="539c246f4c360ac476e128cfbfa84348fb7f7e73" name="mux_3" x="434" y="224">
      <params/>
      <attribs/>
   </obj>
   <obj type="patch/outlet a" sha="9e7e04867e1d37837b0924c9bf18c44ac68602e6" uuid="abd8c5fd3b0524a6630f65cad6dc27f6c58e2a3e" name="Audio Out Right" x="532" y="238">
      <params/>
      <attribs/>
   </obj>
   <nets>
      <net>
         <source obj="Effect ON" outlet="o"/>
         <dest obj="demux_1" inlet="s"/>
         <dest obj="mux_2" inlet="s"/>
         <dest obj="mux_3" inlet="s"/>
      </net>
      <net>
         <source obj="Speed" outlet="wave"/>
         <dest obj="pan_1" inlet="c"/>
      </net>
      <net>
         <source obj="demux_1" outlet="o1"/>
         <dest obj="pan_1" inlet="i1"/>
      </net>
      <net>
         <source obj="demux_1" outlet="o0"/>
         <dest obj="mux_2" inlet="i1"/>
         <dest obj="mux_3" inlet="i1"/>
      </net>
      <net>
         <source obj="pan_1" outlet="left"/>
         <dest obj="mux_2" inlet="i2"/>
      </net>
      <net>
         <source obj="pan_1" outlet="right"/>
         <dest obj="mux_3" inlet="i2"/>
      </net>
      <net>
         <source obj="Audio In" outlet="inlet"/>
         <dest obj="demux_1" inlet="i"/>
      </net>
      <net>
         <source obj="mux_2" outlet="o"/>
         <dest obj="Audio Out Left" inlet="outlet"/>
      </net>
      <net>
         <source obj="mux_3" outlet="o"/>
         <dest obj="Audio Out Right" inlet="outlet"/>
      </net>
   </nets>
   <settings>
      <subpatchmode>no</subpatchmode>
      <MidiChannel>1</MidiChannel>
      <NPresets>8</NPresets>
      <NPresetEntries>32</NPresetEntries>
      <NModulationSources>8</NModulationSources>
      <NModulationTargetsPerSource>8</NModulationTargetsPerSource>
      <Author>Jan Vantomme</Author>
      <License>LGPL</License>
   </settings>
   <notes><![CDATA[]]></notes>
   <windowPos>
      <x>215</x>
      <y>147</y>
      <width>1356</width>
      <height>800</height>
   </windowPos>
</patch-1.0>