<patch-1.0>
   <obj type="ctrl/toggle" sha="f5742cc9eee76fae90a4e570c34596dd327b6c28" uuid="42b8134fa729d54bfc8d62d6ef3fa99498c1de99" name="Effect ON" x="224" y="42">
      <params>
         <bool32.tgl name="b" onParent="true" value="0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="ctrl/i radio 4 v" sha="37aa6f17297981ccdf9a037eecf728e7c0ff0ae4" uuid="b610704137c90b0e43464440b84bfb4fb7d2bb30" name="Number of Bits" x="224" y="112">
      <params>
         <int32.vradio name="value" onParent="true" value="0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="patch/inlet a" sha="709c10aa648c6e5a3c00da4b5dd238899a7c109c" uuid="b577fe41e0a6bc7b5502ce33cb8a3129e2e28ee5" name="Audio In" x="14" y="182">
      <params/>
      <attribs/>
   </obj>
   <obj type="math/gain" sha="cda56a9f0ea80746e47be1a82a59ebc55bdba5d1" uuid="62b1c1a6337c7c8f6aec96408a432477b113cfa0" name="Gain" x="98" y="182">
      <params>
         <frac32.u.map name="amp" onParent="true" value="20.0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="math/quantize" sha="9dea6860d04c1ab0b7e0abcfca2b4b454a79b76a" uuid="c2e999efc45cf8c5077dd1b140dcc793221e050e" name="quantize_1" x="224" y="266">
      <params/>
      <attribs>
         <spinner attributeName="bits" value="1"/>
      </attribs>
   </obj>
   <obj type="mux/mux 4" sha="9f7f3b7a0abf760b335371219c835086f87c62b0" uuid="e511105cf5630d1a0b4a144dc3fabb3cc7c07bd" name="mux_3" x="350" y="280">
      <params/>
      <attribs/>
   </obj>
   <obj type="math/*c" sha="a73a2cafcf92eb4b6524655dcee98a569d5ddc28" uuid="7a66f52a9594e7e9eb31328ea725cb3641a80b55" name="Level" x="448" y="322">
      <params>
         <frac32.u.map name="amp" onParent="true" value="12.0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="math/quantize" sha="9dea6860d04c1ab0b7e0abcfca2b4b454a79b76a" uuid="c2e999efc45cf8c5077dd1b140dcc793221e050e" name="quantize_2" x="224" y="336">
      <params/>
      <attribs>
         <spinner attributeName="bits" value="2"/>
      </attribs>
   </obj>
   <obj type="mux/mux 2" sha="10c5b6d774e8c972b6bb863dad23e83034e0990f" uuid="539c246f4c360ac476e128cfbfa84348fb7f7e73" name="mux_1" x="574" y="336">
      <params/>
      <attribs/>
   </obj>
   <obj type="patch/outlet a" sha="9e7e04867e1d37837b0924c9bf18c44ac68602e6" uuid="abd8c5fd3b0524a6630f65cad6dc27f6c58e2a3e" name="Audio Out" x="672" y="350">
      <params/>
      <attribs/>
   </obj>
   <obj type="math/quantize" sha="9dea6860d04c1ab0b7e0abcfca2b4b454a79b76a" uuid="c2e999efc45cf8c5077dd1b140dcc793221e050e" name="quantize_3" x="224" y="406">
      <params/>
      <attribs>
         <spinner attributeName="bits" value="3"/>
      </attribs>
   </obj>
   <obj type="math/quantize" sha="9dea6860d04c1ab0b7e0abcfca2b4b454a79b76a" uuid="c2e999efc45cf8c5077dd1b140dcc793221e050e" name="quantize_4" x="224" y="476">
      <params/>
      <attribs>
         <spinner attributeName="bits" value="4"/>
      </attribs>
   </obj>
   <nets>
      <net>
         <source obj="Effect ON" outlet="o"/>
         <dest obj="mux_1" inlet="s"/>
      </net>
      <net>
         <source obj="Audio In" outlet="inlet"/>
         <dest obj="mux_1" inlet="i1"/>
         <dest obj="Gain" inlet="in"/>
      </net>
      <net>
         <source obj="Level" outlet="out"/>
         <dest obj="mux_1" inlet="i2"/>
      </net>
      <net>
         <source obj="Gain" outlet="out"/>
         <dest obj="quantize_1" inlet="a"/>
         <dest obj="quantize_2" inlet="a"/>
         <dest obj="quantize_3" inlet="a"/>
         <dest obj="quantize_4" inlet="a"/>
      </net>
      <net>
         <source obj="quantize_1" outlet="b"/>
         <dest obj="mux_3" inlet="i0"/>
      </net>
      <net>
         <source obj="quantize_2" outlet="b"/>
         <dest obj="mux_3" inlet="i1"/>
      </net>
      <net>
         <source obj="quantize_3" outlet="b"/>
         <dest obj="mux_3" inlet="i2"/>
      </net>
      <net>
         <source obj="quantize_4" outlet="b"/>
         <dest obj="mux_3" inlet="i3"/>
      </net>
      <net>
         <source obj="mux_3" outlet="o"/>
         <dest obj="Level" inlet="in"/>
      </net>
      <net>
         <source obj="Number of Bits" outlet="out"/>
         <dest obj="mux_3" inlet="s"/>
      </net>
      <net>
         <source obj="mux_1" outlet="o"/>
         <dest obj="Audio Out" inlet="outlet"/>
      </net>
   </nets>
   <settings>
      <subpatchmode>no</subpatchmode>
      <MidiChannel>1</MidiChannel>
      <NPresets>8</NPresets>
      <NPresetEntries>32</NPresetEntries>
      <NModulationSources>8</NModulationSources>
      <NModulationTargetsPerSource>8</NModulationTargetsPerSource>
      <Author>Jan Vantomme</Author>
      <License>LGPL</License>
      <Attributions></Attributions>
   </settings>
   <notes><![CDATA[]]></notes>
   <windowPos>
      <x>95</x>
      <y>22</y>
      <width>1290</width>
      <height>874</height>
   </windowPos>
</patch-1.0>