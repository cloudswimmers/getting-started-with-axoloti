<patch-1.0>
   <obj type="patch/inlet b" sha="17c8e188371661163bfa55cea9974eecb785fb06" uuid="3b0d3eacb5bb978cb05d1372aa2714d5a4790844" name="Trigger" x="14" y="28">
      <params/>
      <attribs/>
   </obj>
   <obj type="env/d" sha="d9f7cfe1295d7bcc550714a18126d4f73c7c8411" uuid="190ae648e41832b41adbedb465317c18a010aefe" name="d_1" x="126" y="42">
      <params>
         <frac32.s.map name="d" value="-13.0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="gain/vca" sha="c904cdd24d65968df2f5796e107db3747dd691a6" uuid="a9f2dcd18043e2f47364e45cb8814f63c2a37c0d" name="vca_1" x="294" y="56">
      <params/>
      <attribs/>
   </obj>
   <obj type="math/muls 16" sha="8981527339bf34a9e864af533a739f4b4d5c9dda" uuid="c72d593cdf22887ca55f6da46ea788d091a21d19" name="muls_1" x="392" y="70">
      <params/>
      <attribs/>
   </obj>
   <obj type="filter/hp m" sha="c3a6ffa90d2d2057bfbd666463b169384503d2eb" uuid="fdba806c3cfd2b7aca3256c733379a06e5811e66" name="hp_1" x="532" y="84">
      <params>
         <frac32.s.map name="pitch" value="51.0"/>
         <frac32.u.map name="reso" value="16.5"/>
      </params>
      <attribs/>
   </obj>
   <obj type="patch/outlet a" sha="9e7e04867e1d37837b0924c9bf18c44ac68602e6" uuid="abd8c5fd3b0524a6630f65cad6dc27f6c58e2a3e" name="Audio Out" x="672" y="112">
      <params/>
      <attribs/>
   </obj>
   <obj type="noise/gaussian" sha="34a80904716df7e3875cee2c3056be75dc47aa9a" uuid="a0f2cae6eafbfa58e1f1cb28e369824c54486f79" name="gaussian_1" x="140" y="140">
      <params/>
      <attribs/>
   </obj>
   <nets>
      <net>
         <source obj="d_1" outlet="env"/>
         <dest obj="vca_1" inlet="v"/>
         <dest obj="hp_1" inlet="pitch"/>
      </net>
      <net>
         <source obj="gaussian_1" outlet="wave"/>
         <dest obj="vca_1" inlet="a"/>
      </net>
      <net>
         <source obj="vca_1" outlet="o"/>
         <dest obj="muls_1" inlet="in"/>
      </net>
      <net>
         <source obj="muls_1" outlet="out"/>
         <dest obj="hp_1" inlet="in"/>
      </net>
      <net>
         <source obj="Trigger" outlet="inlet"/>
         <dest obj="d_1" inlet="trig"/>
      </net>
      <net>
         <source obj="hp_1" outlet="out"/>
         <dest obj="Audio Out" inlet="outlet"/>
      </net>
   </nets>
   <settings>
      <subpatchmode>no</subpatchmode>
      <MidiChannel>1</MidiChannel>
      <NPresets>8</NPresets>
      <NPresetEntries>32</NPresetEntries>
      <NModulationSources>8</NModulationSources>
      <NModulationTargetsPerSource>8</NModulationTargetsPerSource>
      <Author>Jan Vantomme</Author>
      <License>LGPL</License>
   </settings>
   <notes><![CDATA[]]></notes>
   <windowPos>
      <x>68</x>
      <y>157</y>
      <width>1333</width>
      <height>615</height>
   </windowPos>
</patch-1.0>