<patch-1.0>
   <obj type="patch/inlet b" sha="17c8e188371661163bfa55cea9974eecb785fb06" uuid="3b0d3eacb5bb978cb05d1372aa2714d5a4790844" name="Trigger" x="28" y="42">
      <params/>
      <attribs/>
   </obj>
   <obj type="env/d" sha="d9f7cfe1295d7bcc550714a18126d4f73c7c8411" uuid="190ae648e41832b41adbedb465317c18a010aefe" name="d_1" x="168" y="56">
      <params>
         <frac32.s.map name="d" value="-17.0"/>
      </params>
      <attribs/>
   </obj>
   <obj type="gain/vca" sha="c904cdd24d65968df2f5796e107db3747dd691a6" uuid="a9f2dcd18043e2f47364e45cb8814f63c2a37c0d" name="vca_1" x="392" y="112">
      <params/>
      <attribs/>
   </obj>
   <obj type="patch/outlet a" sha="9e7e04867e1d37837b0924c9bf18c44ac68602e6" uuid="abd8c5fd3b0524a6630f65cad6dc27f6c58e2a3e" name="Audio Out" x="518" y="140">
      <params/>
      <attribs/>
   </obj>
   <obj type="osc/sine lin" sha="3a57035097adb60c79dfabdb4e3a3bd5d47791a1" uuid="6a4458d598c9b8634b469d1a6e7f87971b5932f" name="sine_1" x="168" y="154">
      <params>
         <frac32.u.map name="freq" value="4.5"/>
      </params>
      <attribs/>
   </obj>
   <nets>
      <net>
         <source obj="sine_1" outlet="wave"/>
         <dest obj="vca_1" inlet="a"/>
      </net>
      <net>
         <source obj="d_1" outlet="env"/>
         <dest obj="vca_1" inlet="v"/>
         <dest obj="sine_1" inlet="freq"/>
      </net>
      <net>
         <source obj="vca_1" outlet="o"/>
         <dest obj="Audio Out" inlet="outlet"/>
      </net>
      <net>
         <source obj="Trigger" outlet="inlet"/>
         <dest obj="d_1" inlet="trig"/>
      </net>
   </nets>
   <settings>
      <subpatchmode>normal</subpatchmode>
      <MidiChannel>1</MidiChannel>
      <HasMidiChannelSelector>true</HasMidiChannelSelector>
      <NPresets>8</NPresets>
      <NPresetEntries>32</NPresetEntries>
      <NModulationSources>8</NModulationSources>
      <NModulationTargetsPerSource>8</NModulationTargetsPerSource>
      <Author>Jan Vantomme</Author>
      <License>LGPL</License>
   </settings>
   <notes><![CDATA[]]></notes>
   <windowPos>
      <x>306</x>
      <y>223</y>
      <width>994</width>
      <height>526</height>
   </windowPos>
</patch-1.0>