# Appendix B: Glossary


### Bandpass Filter

A filter that lets through only a narrow band of frequencies.

### Hi-pass filter

https://en.wikipedia.org/wiki/Audio_filter

A filter that removes the lower frequencies from a sound. (everything below the cutoff point ???)

### Low-pass filter

Filters out the higher frequencies from a sound.

### Hertz (Hz)

Unit of Frequency, defined as one cycle per second.

### BPM (Beats per Minute)

Measurement of tempo used in music. For example: a song with a tempo of 120 BPM will play a beat every 0.5 seconds.

### Oscillator

See VCO (Voltage Controlled Oscillator)

### Envelope

Also known as an envelope generator.

### MIDI (Musical Instrument Digital Interface)

A technical standard for communication between different musical instruments.

### LFO (Low Frequency Oscillator)

An oscillator that outputs very low frequencies that generally cannot be heard. An LFO is usually used as a control signal to modulate filters or amplifiers.

### Monophonic

A synthesizer that can only play one note at a time.

### Multitimbral


### Paraphonic ???

### Polyphonic

A synthesizer that can play multiple notes at the same time.

### Polyphony

### Patch





### VCA (Voltage Controlled Amplifier)

An amplifier whose gain is set by a control signal. 

### VCF (Voltage Controlled Filter)

A filter whose cutoff frequency is set by a control signal.

### VCO (Voltage Controlled Oscillator)

An oscillator whose output frequency is set by a control voltage.

### Sample Rate 

The number of audio samples per second, measured in Hertz (Hz). The sample rate used by Axoloti is 48000 Hz.

### Sequencer

### Suboscillator: 

(An oscillator that is set at (usually) one octave below the normal oscillator; used for bass effects.)

### Sustain

### Sync: (remove?)

(Provides a way for you to synchronise the device with something else. For example, LFO sync in synthesizers often allows you to clock the beginning of the LFO cycle to key-on timing. Sync is used in the Roland x0x series to allow both sequencers to start at the same time with the same tempo.)

### Noise

Pink / Gaussian / … (describe different types of noise available on Axoloti)

### Velocity

How hard was the key pressed on the keyboard. Used to control the volume of the sound.

### Pitch

### PWM (Pulse Width Modulation)

### Resonance

### Multiplexer

### Demultiplexer


### Gate

Opens when you press a key on the keyboard, closes when you release the key.

### Trigger

(Similar to a gate, but only) opens for a very short time.


## References

http://www.markwhite.com/vsp/glossary.html
http://www.vintagesynth.com/resources/glossary.php
http://www.modularsynth.co.uk/glossary.shtml