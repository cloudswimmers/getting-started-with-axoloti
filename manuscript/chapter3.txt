# Chapter 3 - Diving into Sound Design

![](images/chapter-3/chapter-003-intro.jpg)

Now that you’ve learned the basics of patching in the first two chapters, it’s time to use that knowledge and create sounds you can use to make music. You’ll learn how to create some classic sounds you can find on almost any synthesizer: bass, lead & pads. These sounds are the main ingredients for just about any kind of electronic music. This chapter will only teach you the basics, but once you understand them, it will be easy to explore the wonderful world of sound design by yourself. In the final part of this chapter, we’ll take a look at how you can create a synthesizer for experimental performances.

## Creating a Bass Synth

There are many ways to create a bass sound. You can use just about any type of oscillator to do so. On a classic bass synth like the Roland TB-303, you can choose between a sawtooth and square oscillator. But you can easily use a sine wave oscillator to create a fat bass sound.

Time to start patching. Create a new document, add the following objects, and arrange them just like _Figure 3.1_. 

* midi/in/keyb
* env/adsr
* osc/sine (2x)
* gain/vca (4x)
* mix/mix 2 g
* math/*c
* audio/stereo out

![Figure 3.1 — The first objects needed to create a bass synthesizer.](images/chapter-3/001-bass-synthesizer-start.png)

1. Connect the gate outlet of the __midi/in/keyb__ object to the inlet of the adsr envelope.
2. Connect the note output of the __midi/in/keyb__ object to the inlets of both sine oscillators.
3. Tune the first oscillator to C3, and the second one to C2.
4. Connect the outlet of the adsr envelope to the blue inlet of the first and the third __gain/vca__ objects.
5. Connect the velocity outlet of the __midi/in/keyb__ object to the blue inlet of the second and fourth __gain/vca__ objects.
6. Connect the outlet of the first oscillator to the red inlet of the first __gain/vca__ object.
7. Connect the outlet of the second oscillator to the red inlet of the third __gain/vca__ object.
8. Connect the outlet of the first __gain/vca__ object to the red inlet of the second __gain/vca__ object.
9. Connect the outlet of the third __gain/vca__ object to the red inlet of the fourth __gain/vca__ object.
10. Connect the outlet of the second __gain/vca__ to the __in1__ inlet of the __mix 2 g__ object.
11. Connect the outlet of the fourth __gain/vca__ to the __in2__ inlet of the __mix 2 g__ object.
12. Connect the outlet of the __mix 2 g__ object to the inlet of the __math/*c__ object.
13. And finally, connect the outlet of the __math/*c__ object to both inlets of the __audio/out stereo__ object.

Your patch should now look like _Figure 3.2_. It’s a good time to save your document, and play your newly created patch to hear what it sounds like.

![Figure 3.2 — All objects for the bass synthesizer are connected.](images/chapter-3/002-bass-synthesizer-step2.png)

Now that you have created a basic bassline, it’s time to make it sound a bit more interesting. Add the following objects to your patch, and arrange them like _Figure 3.3_.

* math/muls 16, the version with the red inlets.
* filter/vcf3
* mux/mux 2, the version with the red inlets.
* ctrl/toggle

![Figure 3.3 — Extra objects to make the sound a bit more interesting.](images/chapter-3/003-bass-synthesizer-extra-objects.png)

Time to connect the new objects you’ve created.

1. Remove the connection between the __mix 2 g__ and the __math/*c__ object.
2. Connect the outlet of the __mix 2 g__ object to the inlet of the __muls 16__ object.
3. Connect the outlet of the __muls 16__ object to the __i1__ inlet of the __mux 2__ object.
4. Connect the outlet of the __muls 16__ object to the red inlet of the __vcf3__ object.
5. Connect the outlet of the __vcf3__ object to the __i2__ inlet of the __mux 2__ object.
6. Connect the outlet of the __mux 2__ object to the inlet of the __math/*c__ object.
7. Connect the outlet of the __ctrl/toggle__ object to the __s__ inlet of the __mux 2__ object.

Your patch is now finished, and it should look like _Figure 3.4_. You can start playing some music now. You can use the toggle object to turn the filter on or off.

![Figure 3.4 — The finished bass synthesizer.](images/chapter-3/004-bass-synthesizer-finished.png)

### The mux object

The multiplexer (or mux), is an object that takes multiple inputs, and forwards one of them to the output. In the bass synth you’ve just built, the object is used to switch between the regular sound and the filtered sound.

Let’s take a look at how this object works. The s input of the mix object takes a boolean value. If the value is 0, the signal of the i1 inlet is forwarded to the outlet. If the value is 1, the signal of the i2 inlet is forwarded to the outlet. The toggle object sends out a value of 0 when it’s turned off, and a value of 1 when it’s turned on. In Figure 3.5, there is a sine oscillator and a saw oscillator connected to the mux 2 object. The disp/i object is used to show the value of the toggle object. The scope 128 b object displays the waveform of the oscillator. You can see that the scope displays the sine wave when the toggle is off, and the sawtooth wave when the toggle is on.

![Figure 3.5 — The mux object lets the sine wave through when the toggle is off, and the saw wave when the toggle is on.](images/chapter-3/005-mux-object-explanation.png)

## Creating a Sub-Bass Synth

If you make techno, dubstep, or hip hop, this patch will come in handy. Sub-bass can be used to add some energy between your kick drums, or to add an extra low version of your main bassline to make your track a bit more punchy when it’s played in a club.

Create a new document, create the following objects, and arrange them like just like _Figure 3.6_.

* midi/in/keyb
* env/adsr
* osc/sine (2x)
* gain/vca (2x)
* mix/mix 2
* filter/lp
* math/*c
* audio/out stereo

![Figure 3.6 — All objects needed to create a sub-bass patch.](images/chapter-3/subbass-start.png)

1. Connect the __note__ outlet of the __midi/in/keyb__ object to the __pitch__ inlet of both __osc/sine__ objects.
2. Connect the __gate__ outlet of the __midi/in/keyb__ object to the inlet of the __env/adsr__ object.
3. Connect the outlet of the __env/adsr__ object to the blue inlet of both __gain/vca__ objects.
4. Connect the outlet of the first __osc/sine__ object to the red inlet of the first __gain/vca__ object.
5. Connect the outlet of the first __osc/sine__ to the __phase__ inlet of the second __osc/sine__ object.
6. Connect the outlet of the second __osc/sine__ object to the red inlet of the second __gain/vca__ object.
7. Connect the outlet of the first __gain/vca__ object to the __in1__ inlet of the __mix/mix 2__ object.
8. Connect the outlet of the second __gain/vca__ object to the __in2__ inlet of the __mix/mix 2__ object.
9. Connect the outlet of the __mix/mix 2__ object to the inlet of the __filter/lp__ object.
10. Connect the outlet of the outlet of the __filter/lp__ object to the inlet of the __math/*c__ object.
11. Connect the outlet of the __math/*c__ object to both inlets of the __audio/out stereo__ object.

If you did everything correctly, your patch should now look like _Figure 3.7_.

![Figure 3.7 — The finished sub-bass patch.](images/chapter-3/subbass-finished.png)

### Tuning Parameters

Set the attack dial of the envelope to -64.00, as the sound should start immediately when you press a key. The sustain level dial should be set to 64.00, so the sound doesn’t stays loud while you play a note. There is some room to can play with the decay and release time, so tune them as you like.

Tune the first oscillator to C2, and the second one to C1. Since this is a sub-bass patch, you want low frequencies.

Tune the resonance of the filter to a low value. Tweak the pitch dial while playing music. If you turn it to the left, the sound will be quite soft. Turning it to the right will result in a more gritty bassline.

## Creating a Lead Synth

Just about any synthesizer made since the early eighties comes with presets. In those preset banks, you’ll usually find a “Lead“ category. A lead synth sound can be just about anything, and is mostly used for the main melody in a track.

Create a new document, create the following objects, and arrange them like just like _Figure 3.8_.

* midi/in/keyb
* env/adsr
* osc/saw (3x)
* mix/mix 3, the one with the red inlets
* gain/vca (2x)
* filter/vcf3
* ctrl/toggle
* mux/mux 2, the one with the red inlets
* math/*c
* audio/out stereo

![Figure 3.8 — All objects needed to create a lead sound.](images/chapter-3/lead-001.png)

1. Connect the __note__ outlet of the __midi/in/keyb__ object to the inlet of the three __osc/saw__ objects.
2. Connect the __gate__ outlet of the __midi/in/keyb__ object to the inlet of the __env/adsr__ object.
3. Connect the __velocity__ outlet of the __midi/in/keyb__ object to the blue inlet of the second __gain/vca__ object.
4. Connect the outlet of the __env/adsr__ object to the blue inlet of the first __gain/vca__ object.
5. Connect the outlets of the __osc/saw__ objects to the __in1__, __in2__ and __in3__ inlets of the __mix 3__ object.
6. Connect the outlet of the __mix 3__ object to the red inlet of the first __gain/vca__ object.
7. Connect the outlet of the first __gain/vca__ object to the red inlet of the second __gain/vca__ object.
8. Connect the outlet of the second __gain/vca__ object to the red inlet of the __filter/vcf3__ object, and to the __i1__ inlet of the __mux 2__ object.
9. Connect the outlet of the __filter/vcf3__ object to the __i2__ inlet of the __mux 2__ object.
10. Connect the outlet of the __ctrl/toggle__ object to the __s__ inlet of the __mux 2__ object.
11. Connect the outlet of the __mux 2__ object to the inlet of the __math/*c__ object.
12. Connect the outlet of the __math/*c__ object to both inlets of the __audio/out stereo__ object.

If you did everything correctly, your patch should now look like _Figure 3.9_.

![Figure 3.9 — The finished lead sound.](images/chapter-3/lead-002.png)

### Tuning Parameters

Lead sounds usually start immediately when you press a key on your midi keyboard, so set the attack dial of the envelope to -64. The decay and release time should be short, the sustain level can be high.

Tune the first oscillator to C2, the second one to C3 and the third one to G5. This will create a sound that has some low bass frequencies, as well as some higher frequencies.

One of the most fun things to play with while you are making music is the filter. You can use the ctrl/toggle object to turn it on/off. Turning the pitch dial to the left will make your sound a lot softer. When you turn it to the right, you can really make your melody scream. 

## Creating a Pad Sound

Pad sounds are probably best known from being overused in cheesy 80s synthpop, and film scores by Vangelis and Jan Hammer. But they can add some character to your music if you use them in a subtle way. Let’s take a look at how you can create them.

Create a new document, create the following objects, and arrange them like just like _Figure 3.10_.

* midi/in/keyb
* env/adsr
* osc/saw (2x)
* gain/vca (4x)
* mix/mix 2 g, the one with the red inlets
* filter/bp svf m
* math/*c, the one with the red inlet
* audio/out stereo

![Figure 3.10 — All objects needed to create a pad sound.](images/chapter-3/pads-001.png)

1. Connect the gate output of the __midi/keyb/in__ object to the inlet of the __env/adsr__ object.
2. Connect the note outlet of the __midi/keyb/in__ object to the inlet of both __osc/saw__ objects.
3. Connect the note outlet of the __midi/keyb/in__ object to the pitch inlet of the __filter/bp svf__ m object
4. Connect the __velocity__ outlet of the __midi/keyb/in__ object to the blue inlet of the second and fourth __gain/vca__ objects.
5. Connect the outlet of the __env/adsr__ object to the blue inlet of the first and third __gain/vca__ objects.
6. Connect the outlet of the first __osc/saw__ object to the red inlet of the first __gain/vca__ object.
7. Connect the outlet of the second __osc/saw__ object to the red inlet of the third __gain/vca__ object.
8. Connect the outlet of the first __gain/vca__ object to the red inlet of the second __gain/vca__ object.
9. Connect the outlet of the third __gain/vca__ object to the red inlet of the fourth __gain/vca__ object.
10. Connect the outlet of the second __gain/vca__ object to the __in1__ inlet of the __mix 2__ object.
11. Connect the outlet of the fourth __gain/vca__ object to the __in2__ inlet of the __mix 2__ object.
12. Connect the outlet of the __mix 2__ object to the red inlet of the __filter/bp svf m__ object.
13. Connect the outlet of the __filter/bp svf m__ object to the inlet of the __math/*c__ object.
14. Connect the outlet of the __math/*c__ object to both inlets of the __audio/out stereo__ object.

If you did everything correctly, your patch should now look like _Figure 3.11_. 

![Figure 3.11 — The finished pad sound.](images/chapter-3/pads-002.png)

### Tuning Parameters

Set all the dials of the ADSR envelope to a value around 40. It doesn’t have to be perfect, just make sure your envelope has a long attack, decay and release time, and a high sustain level. The envelope will ensure that your sound lasts a long time, even if you play a short note.

Tune the first saw oscillator to E4, and the second one to C4. These notes will sound great together when the audio signal of both oscillators is mixed.

Play with the gain levels on the mixer. You can create a more subtle pad sound if only one of the gains is set at the maximum, and the other is set a bit lower.

Finally, set the pitch dial of the filter to -15. The filter is used to make the sound of the sawtooth oscillators much softer. You can play with the resonance a bit, but this won’t make any big changes to your sound.

## Creating an Experimental Drone Synth

So far, you’ve learned how to create sounds that are useful for dance or pop music. This part will be about exploring the other side of electronic music. You’ll learn how to build a drone synth. This patch can be used to play a single note for a few minutes, and gently tweak some parameters over time to create a minimalist piece of music.

Create a new document, create the following objects, and arrange them like just like _Figure 3.12_.

* midi/in/keyb
* env/adsr
* osc/tri cheap (3x)
* osc/phasor compl
* math/+ (3x)
* math/muls 16
* math/muls 64
* gain/vca (2x)
* filter/vcf3
* ctrl/toggle
* mux/mux 2
* math/*c
* audio/out stereo

![Figure 3.12 — All objects needed to create a drone synth.](images/chapter-3/drone-start.png)

1. Connect the __note__ outlet of the __midi/in/keyb__ object to the inlets of the three __osc/tri cheap__ objects.
2. Connect the __note__ outlet of the __midi/in/keyb__ object to the __pitch__ inlet of the __osc/phasor compl__ object.
3. Connect the gate outlet of the __midi/in/keyb__ object to the inlet of the __env/adsr__ object.
4. Connect the outlet of the __env/adsr__ object to the blue inlet of the first __gain/vca__ object.
5. Connect the __velocity__ outlet of the __midi/in/keyb__ object to the blue inlet of the second __gain/vca__ object.
6. Connect the outlet of the first __osc/tri cheap__ object to the __freq__ inlet of the __osc/phasor compl__ object.
7. Connect the __phasor0__ outlet of the __osc/phasor compl__ object to the __in1__ inlet of the first __math/+__ object.
8. Connect the __phasor180__ outlet of the __osc/phasor compl__ object to the __in1__ inlet of the second __math/+__ object.
9. Connect the outlet of the second __osc/tri cheap__ object to the __in2__ inlet of the first __math/+__ object.
10. Connect the outlet of the third __osc/tri cheap__ object to the __in2__ inlet of the second __math/+__ object.
11. Connect the outlet of the first __math/+__ object to the inlet of the __math/muls 16__ object.
12. Connect the outlet of the second __math/+__ object to the inlet of the __math/muls64__ object.
13. Connect the outlet of the __math/muls 16__ object to the __in1__ inlet of the third __math/+__ object.
14. Connect the outlet of the __math/muls 64__ object to the __in2__ inlet of the third __math/+__ object.
15. Connect the outlet of the third __math/+__ object to the red inlet of the first __gain/vca__ object.
16. Connect the outlet of the first __gain/vca__ object to the red inlet of the second __gain/vca__ object.
17. Connect the outlet of the second __gain/vca__ object to the red inlet of the __filter/vcf3__ object.
18. Connect the outlet of the second __gain/vca__ object to the __i1__ inlet of the __mux/mux 2__ object.
19. Connect the outlet of the __filter/vcf3__ object to the __i2__ inlet of the __mux/mux 2__ object.
20. Connect the outlet of the __ctrl/toggle__ object to the __s__ inlet of the __mux/mux__ 2 object.
21. Connect the outlet of the __mux/mux__ 2 object to the inlet of the __math/*c__ object.
22. Connect the outlet of the __math/*c__ object to both inlets of the __audio/out stereo__ object.

If you did everything correctly, your patch should now look like _Figure 3.13_.

![Figure 3.13 — The finished drone sound.](images/chapter-3/drone-finished.png)

### Playing the Instrument

Since this is a really experimental instrument, there is no need to tune the envelope or oscillators. You can create weird soundscapes by playing a single note for a few minutes, and gently rotating any of the dials in the patch. The built-in keyboard window in the Axoloti patcher is ideal for this. Press a note on the keyboard while holding the shift key, and the note will keep playing.

## Exercises

* Create a bass sound with a square or sawtooth oscillator.
* Create a polyphonic subpatch from the lead sound.
* Add a second ADSR envelope to the pad sound. You can make the pad sound richer if each oscillator has its own envelope.
* Create a polyphonic subpatch from the pad sound.
* In the drone synth patch, add an LFO for the pitch, and one for the resonance of the filter to create even weirder sounds.
