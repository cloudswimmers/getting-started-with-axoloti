# Getting Started with Axoloti

* **manuscript:** folder containing text files and images to generate the PDF, ePub and Kindle versions of the book;
* **patches:** all patches used to create the tutorials and screenshots in the book.
* **dist:** all patches and samples to distribute as an extra package for the book. 

## Markua

This book is written in markua, a flavor of markdown with some extras added specifically for writing books on leanpub.

https://leanpub.com/markua/read